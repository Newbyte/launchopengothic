use std::{
    future::Future,
    path::{Path, PathBuf},
    pin::Pin,
};

use crate::PROGRAM_NAME;
use anyhow::{anyhow, Result};
use ashpd::desktop::file_chooser::SelectedFiles;
use urlencoding::decode;

pub fn ask_for_gothic_path() -> Pin<Box<dyn Future<Output = Result<PathBuf>> + Send>> {
    Box::pin(async {
        let title = format!("{} - Pick the directory containing Gothic", PROGRAM_NAME);
        let gothic_install_path = ask_for_path(title.as_str(), true).await?;

        Ok(gothic_install_path)
    })
}

pub fn ask_for_opengothic_path() -> Pin<Box<dyn Future<Output = Result<PathBuf>> + Send>> {
    Box::pin(async {
        let title = format!("{} - Pick the OpenGothic executable", PROGRAM_NAME);
        let opengothic_path = ask_for_path(title.as_str(), false).await?;

        Ok(opengothic_path)
    })
}

async fn ask_for_path(title: &str, directory: bool) -> Result<PathBuf> {
    let files = SelectedFiles::open_file()
        .title(title)
        .accept_label("read")
        .modal(true)
        .multiple(false)
        .directory(directory)
        .send()
        .await?
        .response()?;

    let uris = &files.uris();

    if uris.is_empty() {
        return Err(anyhow!("No file selected!"));
    }

    let path_str = uris[0].path();

    let path_decoded = decode(path_str)?.to_string();

    let path = Path::new(&path_decoded);

    if !(path.try_exists()?) {
        return Err(anyhow!("Path '{:?}' does not exist", path));
    }

    if directory && !path.is_dir() {
        return Err(anyhow!("No Directory selected!"));
    }

    if !directory && !path.is_file() {
        return Err(anyhow!("No File selected!"));
    }

    Ok(path.to_owned())
}
