use anyhow::{anyhow, Result};
use std::{
    future::Future,
    path::PathBuf,
    pin::Pin,
    sync::{Arc, Mutex},
};

use eframe::egui::{self, Align, FontId, Layout, Ui, Vec2};

use crate::{
    user_interaction::{ask_for_gothic_path, ask_for_opengothic_path},
    PROGRAM_NAME,
};

#[derive(Debug, Clone)]
pub enum LaunchDecision {
    Start,
    Cancel,
}

#[derive(Debug)]
struct DecisionDialog {
    decision: Arc<Mutex<LaunchDecision>>,
    opengothic_path: Arc<Mutex<Option<PathBuf>>>,
    gothic_path: Arc<Mutex<Option<PathBuf>>>,
    opengothic_selectable: bool,
}

impl DecisionDialog {
    fn new(
        _cc: &eframe::CreationContext<'_>,
        default_decision: Arc<Mutex<LaunchDecision>>,
        opengothic_path: Arc<Mutex<Option<PathBuf>>>,
        gothic_path: Arc<Mutex<Option<PathBuf>>>,
        opengothic_selectable: bool,
    ) -> Self {
        Self {
            decision: default_decision,
            opengothic_path,
            gothic_path,
            opengothic_selectable,
        }
    }
}

type FutFn = fn() -> Pin<Box<dyn Future<Output = Result<PathBuf>> + Send>>;

const FONT_SIZE: f32 = 24.0;

impl eframe::App for DecisionDialog {
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            render_decisions(ui, self);
        });
        egui::TopBottomPanel::bottom("bottom_panel").show(ctx, |ui| {
            render_buttons(ui, self, frame);
        });
    }
}

fn get_remaining_horizontal_space(ui: &Ui) -> f32 {
    let max_width = ui.available_size_before_wrap().x;
    let cursor_pos = ui.cursor();
    max_width - cursor_pos.min.x
}

fn render_path_input(
    ui: &mut Ui,
    path_arc: Arc<Mutex<Option<PathBuf>>>,
    button_text: &str,
    error_text: &str,
    get_path: FutFn,
) {
    if let Ok(path_guard) = path_arc.lock() {
        let mut text = match *path_guard {
            Some(ref path_buf) => path_buf.display().to_string(),
            None => "".to_string(),
        };

        ui.horizontal(|ui| {
            let remaining_space = get_remaining_horizontal_space(ui);

            ui.add_sized(
                egui::Vec2::new(remaining_space - 200.0, FONT_SIZE),
                egui::TextEdit::singleline(&mut text),
            );
            if ui.button(button_text).clicked() {
                let path_arc = Arc::clone(&path_arc);
                let error_text = error_text.to_owned();
                tokio::spawn(async move {
                    match get_path().await {
                        Ok(path) => {
                            if let Ok(mut path_guard) = path_arc.lock() {
                                *path_guard = Some(path);
                            }
                        }
                        Err(e) => eprintln!("{}: {:?}", error_text, e),
                    }
                });
            };
        });
    }
}

fn render_decisions(ui: &mut Ui, dialog: &DecisionDialog) {
    ui.with_layout(Layout::top_down(Align::Min), |ui| {
        ui.style_mut().text_styles.insert(
            egui::TextStyle::Body,
            FontId::new(FONT_SIZE, eframe::epaint::FontFamily::Proportional),
        );
        ui.style_mut().text_styles.insert(
            egui::TextStyle::Button,
            FontId::new(FONT_SIZE, eframe::epaint::FontFamily::Proportional),
        );
        if dialog.opengothic_selectable {
            render_path_input(
                ui,
                Arc::clone(&dialog.opengothic_path),
                "OpenGothic Path",
                "Error getting OpenGothic path",
                ask_for_opengothic_path,
            );
        }
        render_path_input(
            ui,
            Arc::clone(&dialog.gothic_path),
            "Gothic Path",
            "Error getting Gothic path",
            ask_for_gothic_path,
        );
    });
}

fn is_launchable(dialog: &DecisionDialog) -> bool {
    let mut launchable = true;
    if let Ok(og_path) = dialog.opengothic_path.lock() {
        launchable = match *og_path {
            Some(ref og_path) => og_path.is_file(),
            None => false,
        };
    }
    if let Ok(g_path) = dialog.gothic_path.lock() {
        launchable = launchable
            && match *g_path {
                Some(ref g_path) => g_path.is_dir(),
                None => false,
            };
    }
    launchable
}

fn render_buttons(ui: &mut Ui, dialog: &mut DecisionDialog, frame: &mut eframe::Frame) {
    ui.with_layout(Layout::right_to_left(Align::Center), |ui| {
        ui.style_mut().text_styles.insert(
            egui::TextStyle::Button,
            FontId::new(FONT_SIZE, eframe::epaint::FontFamily::Proportional),
        );
        if ui
            .add_enabled(
                is_launchable(dialog),
                egui::Button::new("Launch OpenGothic"),
            )
            .clicked()
        {
            if let Ok(mut decision) = dialog.decision.lock() {
                *decision = LaunchDecision::Start
            }
            frame.close();
        }
        if ui.button("Cancel").clicked() {
            if let Ok(mut decision) = dialog.decision.lock() {
                *decision = LaunchDecision::Cancel
            }
            frame.close();
        }
    });
}

fn unwrap_arc_mutex<T>(decision: Arc<Mutex<T>>) -> Result<T>
where
    T: Clone,
{
    if let Ok(decision) = decision.lock() {
        Ok(decision.clone())
    } else {
        Err(anyhow!("Error getting decision."))
    }
}

pub fn show_dialog(
    opengothic_path: &Option<PathBuf>,
    gothic_path: &Option<PathBuf>,
    opengothic_path_selectable: bool,
) -> Result<(LaunchDecision, Option<PathBuf>, Option<PathBuf>)> {
    let native_options = eframe::NativeOptions {
        initial_window_size: Some(Vec2 { x: 800.0, y: 200.0 }),
        resizable: false,
        ..Default::default()
    };

    let decision = Arc::new(Mutex::new(LaunchDecision::Cancel));

    let decision_clone = Arc::clone(&decision);

    let og_path = Arc::new(Mutex::new(opengothic_path.clone()));
    let og_path_clone = Arc::clone(&og_path);

    let g_path = Arc::new(Mutex::new(gothic_path.clone()));
    let g_path_clone = Arc::clone(&g_path);

    let _ = eframe::run_native(
        PROGRAM_NAME,
        native_options,
        Box::new(move |cc| {
            Box::new(DecisionDialog::new(
                cc,
                decision_clone,
                og_path_clone,
                g_path_clone,
                opengothic_path_selectable,
            ))
        }),
    );

    Ok((
        unwrap_arc_mutex(decision)?,
        unwrap_arc_mutex(og_path)?,
        unwrap_arc_mutex(g_path)?,
    ))
}
