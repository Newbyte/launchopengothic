use anyhow::{anyhow, Result};
use serde::{Deserialize, Serialize};
use std::{env, fs, path::PathBuf};

use crate::PACKAGE_NAME;

#[derive(Deserialize, Serialize, Debug)]
pub struct Settings {
    pub opengothic_path: Option<PathBuf>,
    pub gothic_install_path: Option<PathBuf>,
}

fn get_config_path() -> Result<PathBuf> {
    match env::var("XDG_DATA_HOME") {
        Ok(path) => Ok(PathBuf::from(
            format!("{}/{}/config.toml", path, PACKAGE_NAME).as_str(),
        )),
        Err(_) => {
            let home_dir = env::var("HOME")?;
            let mut config = PathBuf::from(home_dir);
            config.push(format!(".local/share/{}/config.toml", PACKAGE_NAME));
            Ok(config)
        }
    }
}

pub fn get_settings() -> Result<Option<Settings>> {
    let config_file = get_config_path()?;

    if !config_file.exists() {
        return Ok(None);
    }

    if !config_file.is_file() {
        panic!("Config file exists, but is not a file!");
    }

    let config_str = fs::read_to_string(config_file)?;
    Ok(Some(toml::from_str(&config_str)?))
}

pub fn set_settings(settings: &Settings) -> Result<()> {
    let config_file = get_config_path()?;

    let parent_path = config_file.parent();

    match parent_path {
        Some(path) => {
            if !path.try_exists()? {
                fs::create_dir_all(path)?;
            }
        }
        None => return Err(anyhow!("config file has no parent!")),
    }

    if config_file.try_exists()? {
        fs::remove_file(&config_file)?;
    }

    let settings_str = toml::to_string(settings)?;

    fs::write(&config_file, settings_str)?;

    Ok(())
}
