use anyhow::Result;
use clap::Parser;
use launch_dialog::show_dialog;
use settings::{get_settings, set_settings, Settings};
use std::{path::PathBuf, process::Command};

mod launch_dialog;
mod settings;
mod user_interaction;

const PROGRAM_NAME: &str = "Launch OpenGothic";
const PACKAGE_NAME: &str = env!("CARGO_PKG_NAME");

#[derive(Debug, Parser)]
#[command(name = PACKAGE_NAME)]
#[command(about = "Launcher for OpenGothic", long_about = None)]
struct Cli {
    #[arg(short, long)]
    opengothic_path: Option<PathBuf>,
}

#[tokio::main()]
async fn main() -> Result<()> {
    let args = Cli::parse();

    let ask_og_path = args.opengothic_path.is_none();

    let settings = get_settings()?;

    let opengothic_path = match args.opengothic_path {
        Some(og_path) => Some(og_path),
        None => match settings {
            Some(ref set) => set.opengothic_path.clone(),
            None => None,
        },
    };

    let gothic_path = match settings {
        Some(set) => set.gothic_install_path,
        None => None,
    };

    let (decision, og_path, g_path) = show_dialog(&opengothic_path, &gothic_path, ask_og_path)?;

    match decision {
        launch_dialog::LaunchDecision::Start => {
            let error_msg = format!(
                "One or more Paths are missing. Gothic: {:?}, OpenGothic: {:?}",
                g_path, og_path
            );
            if let (Some(og_path), Some(g_path)) = (og_path, g_path) {
                let settings = Settings {
                    opengothic_path: Some(og_path.clone()),
                    gothic_install_path: Some(g_path.clone()),
                };
                set_settings(&settings)?;
                Command::new(og_path)
                    .arg("-g")
                    .arg(g_path)
                    .spawn()
                    .expect("Failed to open OpenGothic");
            } else {
                eprintln!("{}", error_msg);
            }
        }
        launch_dialog::LaunchDecision::Cancel => {}
    }

    Ok(())
}
